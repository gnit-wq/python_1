# =============================================================================
# l1=[10,'Rajat','IT',5.36,5+7j]
# len(l1)
# l2=[20,'List',569.32,'India']
# l1.extend(l2)
# print(l1)
# for i in l1:
#     print(i)
# l1.pop(8)
# print(l1)
# for i in l1:
#     print(i)
# =============================================================================
def fun1(l3):
    for z in l3:
        print(z)
number=[420,520,1025629,564284,654268]
fun1(number)
def fun2(p9):
    for x in p9:
        print(x)
name=['hello','world','hii']
fun2(name)
name.extend(number)
print(name)
for x in name:
    print(x)
    
    #NumPy 1D array
import numpy as np
rajat=np.array([[1, 2, 3], [4, 5, 6]])
for i in rajat:
    print(i) 
    print(type(i))
    #NumPy 2D array
import numpy as np
mainak=np.array([[1, 2, 3], [4, 5, 6],[7,8,9]])
for x in mainak:
    for y in x:
        print(y)
print(type(y))
 
deb=np.array([46,642,625,963,4208])
for z in np.nditer(deb,flags=['buffered'],op_dtypes=['S']):
    print(z)


